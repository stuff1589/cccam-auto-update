from Tkinter import *
import Tkinter
import urllib2
import random
import requests
import re
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from time import sleep
import glob
import os, os.path


MAIN_GRID = Tkinter.Tk()
CCCAM3_SWITCH = BooleanVar()
CCCAM2_SWITCH = BooleanVar()
CCCAM_LUX_SWITCH = BooleanVar( value= True )
CCCAM_EGYPTSAT_SWITCH = BooleanVar(value= True)
NEWCAM_EGYPTSAT_SWITCH = BooleanVar(value= True)
PUBLISH_SWITCH = BooleanVar(value= True)
REMOTE_OUTPUT_PATH = StringVar(MAIN_GRID, value = r'etc/CCcam.cfg')
RAND_USER_NUM = StringVar(MAIN_GRID , value = str(random.randint(1,11)))
EGYPTSAT_USERNAME = 'user'
EGYPTSAT_PASSWORD = 'password'

def close_driver_all_windows(web_driver):
    for handle in web_driver.window_handles:
        web_driver.switch_to.window(handle)
        web_driver.close()



def cccam_lux_parse():
    print r'STARTED http://www.cccamlux.com/Free-CCcam.php'
    c_line =None
    for line in urllib2.urlopen('http://www.cccamlux.com/Free-CCcam.php') :
        matchObj = re.match('.*<strong>(C:.*)</strong>.*', line)
        # = re.match('.*(C:.*).*', line)
        if matchObj:
            c_line = matchObj.group(1)
            c_line = c_line.replace('user:', '')
            c_line = c_line.replace('port:', '')
            c_line = c_line.replace('pass:', '')
            c_line = ' '.join(c_line.split())
    return c_line

#l = cccam_lux_parse()
#print l



def cccam_2_3_parse(start_url):
    print 'STARTED ' + start_url
    c_line = None
    page1 = requests.get(start_url)
    page1.raise_for_status()
    page2_url = None
    for line in page1.iter_lines():
        matchObj = re.match('.*href="(http.*tinyium.*)".*target.*', line)
        if matchObj:
            page2_url = matchObj.group(1)


    if page2_url:
        print 'OPENING '+ page2_url
        driver_cccam = webdriver.Chrome()
        driver_cccam.get(page2_url)
        href = None
        while not href:
            mwButton = driver_cccam.find_elements_by_class_name('mwButton')
            href = mwButton[0].get_attribute('href')
        for line in urllib2.urlopen(href):
            if line.startswith('C:') :
                line_list = line.split()
                line_list[3] = (line_list[3])[:-1]
                line_list[3] = line_list[3] + RAND_USER_NUM.get()
                c_line = ' '.join(line_list)
                break
        close_driver_all_windows(driver_cccam)
    return c_line



def delete_existing_files(pattern):
    for file in glob.glob(pattern):
        print 'removing ' + file
        os.remove(file)


def get_3gyptsat_file_txt_url(start_url) :
    # getting cccam.txt file url to download
    r = requests.get(start_url)
    r.raise_for_status()
    file_url_cccam = None
    for line in r.iter_lines():
        matchObj = re.match('.*="(http.*txt)">.*', line)
        if matchObj:
            file_url = matchObj.group(1)
    return file_url

def run_chrome_with_adblockers():
    # launchng chrome with 3 addblock extensions to prevent ads
    curr_path = os.path.dirname(os.path.realpath('__file__'))
    path_to_extension_1 = curr_path + r'\ad_block_exten\4.3_0'
    path_to_extension_2 = curr_path + r'\ad_block_exten\1.5.2_0'
    path_to_extension_3 = curr_path + r'\ad_block_exten\1.3_0'
    chrome_options = Options()
    chrome_options.add_argument(
        'load-extension=' + path_to_extension_1 + ',' + path_to_extension_2 + ',' + path_to_extension_3)
    chrome_driver = webdriver.Chrome(chrome_options=chrome_options)
    chrome_driver.create_options()
    return chrome_driver

def egyptsat_download_file (chrome_driver,txt_file_url):
    chrome_driver.get(txt_file_url)
    username = chrome_driver.find_element_by_name('vb_login_username')
    username.send_keys(EGYPTSAT_USERNAME)
    password = chrome_driver.find_element_by_name('vb_login_password')
    password.send_keys(EGYPTSAT_PASSWORD + '\n')

def wait_download_finish(path_pattern):
    print 'downloading'
    while not glob.glob(path_pattern):
        print "#",
        sleep(0.1)
    print '\n'

def parse_egyptsat_cccam(path_pattern):
    # parsing the cccam
    c_line = None
    cccam_file_path = glob.glob(path_pattern)[0]
    with open(cccam_file_path) as cccam_file:
        for line in cccam_file:
            if line.startswith('C'):
                line_list = line.split()
                cccam_user = line_list[3]
                cccam_user = cccam_user[:-1] + RAND_USER_NUM.get()
                line_list[3] = cccam_user
                c_line = " ".join(line_list)
                break
    return c_line

def get_egyptsat_cccam():
    print 'STARTED EGYPTSAT CCCAM'
    path_pattern = r'E:\Downloads\cccam*.txt'
    delete_existing_files(path_pattern)
    txt_file_url = get_3gyptsat_file_txt_url('http://www.3gyptsat.net/vb/t4809.html')
    print 'cccam file url'
    print txt_file_url
    chrome_driver = run_chrome_with_adblockers()
    egyptsat_download_file(chrome_driver,txt_file_url)
    wait_download_finish(path_pattern)
    close_driver_all_windows(chrome_driver)
    c_line = parse_egyptsat_cccam(path_pattern)
    return c_line



def parse_egyptsat_newcam(path_pattern):
    # parsing the downladed newcam.txt
    print 'parsing the downladed newcam.txt'
    CAID_LIST =['090D','0604','0500','0930','0100','0B00','1803']
    caid_or_pattern = '|'.join(CAID_LIST)
    user_newcam = ''
    pass_newcam = ''
    newcam_user_found_flag = False
    host_name = ''
    des_key = '01 02 03 04 05 06 07 08 09 10 11 12 13 14'
    port_list = []
    n_lines = []
    newcam_file_path = glob.glob(path_pattern)[0]
    with open(newcam_file_path) as fp:
        for line in fp:
            match_port = re.match(r'(\d+)(.*)'+'(' + caid_or_pattern + ')' , line)
            match_user = re.match(r'(user\s*:\s*)(.*)', line)
            match_pass = re.match(r'(pass\s*:\s*)(.*)', line)
            match_host_name = re.match(r'(hostname\s*:\s*)(.*)', line)
            if match_port:
                port_list.append(match_port.groups(0)[0])
            if match_user and not newcam_user_found_flag:
                newcam_user_found_flag = True
                user_newcam = match_user.groups(0)[1]
                user_newcam = user_newcam.strip()
                user_newcam = user_newcam[:-1]
            if match_pass:
                pass_newcam = match_pass.groups(0)[1]
                pass_newcam = pass_newcam.strip()
            if match_host_name:
                host_name = match_host_name.groups(0)[1]
                host_name = host_name.strip()
    user_newcam_random = user_newcam + RAND_USER_NUM.get()
    for port in port_list:
        n_line = 'N:' + ' ' + host_name + ' ' + port + ' ' + user_newcam_random + ' ' + pass_newcam + ' ' + des_key
        n_lines.append(n_line)

    return n_lines

def get_egyptsat_newcam():
    print 'STARTED EGYPTSAT NEWCAM'
    path_pattern = r'E:\Downloads\newcam*.txt'
    delete_existing_files(path_pattern)
    txt_file_url = get_3gyptsat_file_txt_url('http://www.3gyptsat.net/vb/t4807.html')
    print 'newcam file url'
    print txt_file_url
    chrome_driver = run_chrome_with_adblockers()
    egyptsat_download_file(chrome_driver,txt_file_url)
    wait_download_finish(path_pattern)
    close_driver_all_windows(chrome_driver)
    n_lines = parse_egyptsat_newcam(path_pattern)
    return n_lines




def publish_local(collected_lines_list):

    cccam_config_file = 'CCcam.cfg'
    if not collected_lines_list:
        return cccam_config_file

    if os.path.exists(cccam_config_file):
        os.remove(cccam_config_file)
        print 'removed old local CCcam.cfg'

    print 'Creating new local CCcam.cfg'

    with open(cccam_config_file, 'w+') as f:
        for line in collected_lines_list:
            if line:
                f.write(line + '\n')
    return cccam_config_file

def publish_remote(cccam_config_file):
    print 'sending CCcam.cfg to receiver'
    from ftplib import FTP
    ftp = FTP('192.168.1.15')
    ftp.login('root')
    #ftp.retrlines('LIST')
    with open(cccam_config_file, 'r') as f:
        ftp.storlines('STOR %s' % REMOTE_OUTPUT_PATH.get(), f)
    ftp.quit()

def start():
    collected_lines_list = []
    if (CCCAM3_SWITCH.get()):
        collected_lines_list.append(cccam_2_3_parse(r'http://www.cccam3.com/free-cccam-server/'))

    if (CCCAM2_SWITCH.get()):
        collected_lines_list.append(cccam_2_3_parse(r'http://www.cccam2.com/free-cccam-servers/'))

    if (CCCAM_LUX_SWITCH.get()):
        collected_lines_list.append(cccam_lux_parse())

    if (CCCAM_EGYPTSAT_SWITCH.get()):
        collected_lines_list.append(get_egyptsat_cccam())

    if (NEWCAM_EGYPTSAT_SWITCH.get()):
        collected_lines_list = collected_lines_list + get_egyptsat_newcam()

    local_config_file = publish_local(collected_lines_list)

    if (PUBLISH_SWITCH.get()):
        publish_remote(local_config_file)

    print 'Done'



if __name__ == "__main__":

    cccam3 = Checkbutton(MAIN_GRID, text = "CCCAM3", variable = CCCAM3_SWITCH, \
                     onvalue = True, offvalue = False)

    cccam2 = Checkbutton(MAIN_GRID, text = "CCCAM2", variable = CCCAM2_SWITCH, \
                     onvalue = True, offvalue = False)

    cccam_lux = Checkbutton(MAIN_GRID, text = "CCCAM_LUX", variable = CCCAM_LUX_SWITCH, \
                     onvalue = True, offvalue = False)

    cccam_egyptsat = Checkbutton(MAIN_GRID, text = "CCCAM_EGYPTSAT", variable = CCCAM_EGYPTSAT_SWITCH, \
                     onvalue = True, offvalue = False)

    newcam_egyptsat = Checkbutton(MAIN_GRID, text = "NEWCAM_EGYPTSAT", variable = NEWCAM_EGYPTSAT_SWITCH, \
                     onvalue = True, offvalue = False)

    publish_switch = Checkbutton(MAIN_GRID, text = "Publish to Remote", variable = PUBLISH_SWITCH, \
                     onvalue = True, offvalue = False)

    start_button = Button(text="START", width=15, height = 3, fg="red", command=start )
    start_button.grid(row = 0 , columnspan = 2)
    publish_switch.grid(row = 2 , sticky = W)

    rand_user_label = Label(MAIN_GRID, text="user number").grid(row = 3 , sticky = W)
    rand_user_entry = Entry(MAIN_GRID , textvariable = RAND_USER_NUM ).grid(row = 3 , column = 1)

    path_label = Label(MAIN_GRID, text="output remote path").grid(row = 4 , sticky = W)
    path_entry = Entry(MAIN_GRID , textvariable = REMOTE_OUTPUT_PATH ).grid(row = 4 , column = 1)

    cccam3.grid(row = 6 , sticky = W)
    cccam2.grid(row = 7 , sticky = W)
    cccam_lux.grid(row = 8 , sticky = W)
    cccam_egyptsat.grid(row = 9 , sticky = W)
    newcam_egyptsat.grid(row = 10 , sticky = W)

    MAIN_GRID.mainloop()

